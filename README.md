# Projet AMIO

## Fonctionnalités

 - Récupérez l'état actuel (nom et luminosité) des capteurs avec horodatage
 - Profitez d'une vérification de l'allumage des lumières le soir
	 - Démarrez et arrêtez le service à tout moment
	 - Démarrez le service au démarrage du téléphone
	 - Récupérez l'état du service
	 - Soyez notifier des changements par email et notifications Push
	 - Définissez ou désactivez les plages horaires de vérifications séparément en semaine et en fin de semaine
 - Définissez votre nom d'utilisateur, votre adresse email et les plages horaires de vérification dans les paramètres

## Auteurs

Ce projet a été réalisé par **Florent Caspar** et **Quentin Jacqmin**