package com.example.projetamio;

import static com.example.projetamio.TimePickerFragment.getHourStr;
import static com.example.projetamio.TimePickerFragment.getMinuteStr;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainService extends Service implements IOTLabCalls.Callbacks {
    Timer timer;
    List<Boolean> lightsState;
    int notif_id;
    String CHANNEL_ID = "amio";
    NotificationManager notificationManager;
    Calendar calendar;

    public MainService() {
        lightsState = new ArrayList<>();
        notif_id = 0;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "AMIO", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(mChannel);
        }

        calendar = Calendar.getInstance();
        final Handler handler = new Handler();
        TimerTask task = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        // Toast.makeText(MainService.this, "plop !", Toast.LENGTH_SHORT).show();

                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        int time = 60*calendar.get(Calendar.HOUR_OF_DAY) + calendar.get(Calendar.MINUTE);
                        int day = calendar.get(Calendar.DAY_OF_WEEK);
                        int wdaysFrom = 60*prefs.getInt(getHourStr(TimePickerStep.weekdaysFrom), 0) + prefs.getInt(getMinuteStr(TimePickerStep.weekdaysFrom), 0);
                        int wdaysTo = 60*prefs.getInt(getHourStr(TimePickerStep.weekdaysTo), 0) + prefs.getInt(getMinuteStr(TimePickerStep.weekdaysTo), 0);
                        int wendsFrom = 60*prefs.getInt(getHourStr(TimePickerStep.weekendsFrom), 0) + prefs.getInt(getMinuteStr(TimePickerStep.weekendsFrom), 0);
                        int wendsTo = 60*prefs.getInt(getHourStr(TimePickerStep.weekendsTo), 0) + prefs.getInt(getMinuteStr(TimePickerStep.weekendsTo), 0);
                        if ((day == Calendar.SATURDAY || day == Calendar.SUNDAY) &&
                                (wendsFrom <= time && time < wendsTo || wendsFrom > wendsTo && (time >= wendsFrom || time < wendsTo))
                                || wdaysFrom <= time && time < wdaysTo || wdaysFrom > wdaysTo && (time >= wdaysFrom || time < wdaysTo)) {
                            IOTLabCalls.fetchMotes(MainService.this, 1, "light1");
                        } else {
                            lightsState.clear();
                        }
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(task, 0, 10000);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("MainService", "Création du service");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("MainService", "Destruction du service");
        timer.cancel();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onResponse(@Nullable Motes motes) {
        if (motes != null) {
            if (lightsState.isEmpty()) {
                for (int i = 0; i < motes.getData().size(); i++) {
                    Mote mote = motes.getData().get(i);
                    boolean isOn = mote.getValue() > 250;
                    lightsState.add(isOn);
                    Log.d("MainService", String.format("Mote %s is initially %s (%.2f)", mote.getMote(), isOn ? "on" : "off", mote.getValue()));
                }
            }
            else {
                StringBuilder changes = new StringBuilder();
                for (int i = 0; i < motes.getData().size(); i++) {
                    Mote mote = motes.getData().get(i);
                    boolean isOn = mote.getValue() > 250;
                    if (isOn != lightsState.get(i)) {
                        lightsState.set(i, isOn);
                        if (isOn) {
                            changes.append(String.format("Mote %s was turned %s (%.2f)\n", mote.getMote(), isOn ? "on" : "off", mote.getValue()));
                        }
                    }
                }
                if (changes.length() > 0) {
                    Log.d("MainService", changes.toString());
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "amio")
                            .setSmallIcon(R.drawable.abc_vector_test)
                            .setContentTitle("Light(s) switched on")
                            .setContentText(changes.toString())
                            .setAutoCancel(true);
                    notificationManager.notify(notif_id, builder.build());
                    notif_id++;

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    Intent emailIntent = new Intent(Intent.ACTION_SEND)
                            .putExtra(android.content.Intent.EXTRA_EMAIL, prefs.getString("email", "thibault.cholez@telecomnancy.eu"))
                            .putExtra(android.content.Intent.EXTRA_SUBJECT, "Light(s) switched on")
                            .putExtra(android.content.Intent.EXTRA_TEXT, changes.toString())
                            .setType("text/plain");
                    startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                }
            }
        }
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
    }
}