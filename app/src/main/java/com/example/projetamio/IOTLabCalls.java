package com.example.projetamio;

import android.util.Log;

import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IOTLabCalls {

    // 1 - Creating a callback
    public interface Callbacks {
        void onResponse(@Nullable Motes motes);
        void onFailure();
    }

    // 2 - Public method to start fetching users following by Jake Wharton
    public static void fetchMotes(Callbacks callbacks, int experimentId, String labels){

        // 2.1 - Create a weak reference to callback (avoid memory leaks)
        final WeakReference<Callbacks> callbacksWeakReference = new WeakReference<Callbacks>(callbacks);

        // 2.2 - Get a Retrofit instance and the related endpoints
        IOTLabService iotlabservice = IOTLabService.retrofit.create(IOTLabService.class);

        // 2.3 - Create the call on Github API
        Call<Motes> call = iotlabservice.getExperimentLatestData(experimentId, labels);
        // 2.4 - Start the call
        call.enqueue(new Callback<Motes>() {

            @Override
            public void onResponse(Call<Motes> call, Response<Motes> response) {
                Log.d("IOTLabCalls", String.valueOf(response.code()));
                // 2.5 - Call the proper callback used in controller (MainFragment)
                if (callbacksWeakReference.get() != null) callbacksWeakReference.get().onResponse(response.body());
            }

            @Override
            public void onFailure(Call<Motes> call, Throwable t) {
                Log.d("IOTLabCalls", t.getMessage());
                // 2.5 - Call the proper callback used in controller (MainFragment)
                if (callbacksWeakReference.get() != null) callbacksWeakReference.get().onFailure();
            }
        });
    }
}