package com.example.projetamio;

public enum TimePickerStep {
    weekdaysFrom,
    weekdaysTo,
    weekendsFrom,
    weekendsTo,
}
