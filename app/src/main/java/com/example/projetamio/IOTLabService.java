package com.example.projetamio;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IOTLabService {
    @GET("iotlab/rest/data/{experiment_id}/{labels}/last")
    Call<Motes> getExperimentLatestData(
            @Path("experiment_id") int experimentId,
            @Path("labels") String labels
    );

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://iotlab.telecomnancy.eu:8080/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}