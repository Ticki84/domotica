package com.example.projetamio;

import static com.example.projetamio.TimePickerFragment.getHourStr;
import static com.example.projetamio.TimePickerFragment.getMinuteStr;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import java.sql.Timestamp;

public class MainActivity extends AppCompatActivity implements IOTLabCalls.Callbacks {

    MenuItem settingsItem;

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MainActivity", "Création de l'activité");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        setContentView(R.layout.activity_main);

        TextView text1 = findViewById (R.id.textView2);
        ToggleButton switch1 = findViewById (R.id.toggleButton);
        text1.setText(isServiceRunning(MainService.class) ? "Running" : "Stopped");
        switch1.setChecked(isServiceRunning(MainService.class));
        switch1.setOnClickListener(v -> {
            if (switch1.isChecked()) {
                startService(new Intent(this, MainService.class));
            } else {
                stopService(new Intent(this, MainService.class));
            }
            text1.setText(switch1.isChecked() ? "Running" : "Stopped");
        });

        CheckBox checkbox1 = findViewById(R.id.checkBox);
        checkbox1.setChecked(prefs.getBoolean("serviceOnBoot", false));
        checkbox1.setOnClickListener(v -> {
            Log.d("MainActivity", "Start service at boot switched");
            editor.putBoolean("serviceOnBoot", checkbox1.isChecked());
            editor.apply();
        });

        if (!prefs.contains(getHourStr(TimePickerStep.weekdaysFrom))
                || !prefs.contains(getMinuteStr(TimePickerStep.weekdaysFrom))
                || !prefs.contains(getHourStr(TimePickerStep.weekdaysTo))
                || !prefs.contains(getMinuteStr(TimePickerStep.weekdaysTo))) {
            editor.putInt(getHourStr(TimePickerStep.weekdaysFrom), 23);
            editor.putInt(getMinuteStr(TimePickerStep.weekdaysFrom), 0);
            editor.putInt(getHourStr(TimePickerStep.weekdaysTo), 6);
            editor.putInt(getMinuteStr(TimePickerStep.weekdaysTo), 0);
            editor.apply();
        }
        if (!prefs.contains(getHourStr(TimePickerStep.weekendsFrom))
                || !prefs.contains(getMinuteStr(TimePickerStep.weekendsFrom))
                || !prefs.contains(getHourStr(TimePickerStep.weekendsTo))
                || !prefs.contains(getMinuteStr(TimePickerStep.weekendsTo))) {
            editor.putInt(getHourStr(TimePickerStep.weekendsFrom), 19);
            editor.putInt(getMinuteStr(TimePickerStep.weekendsFrom), 0);
            editor.putInt(getHourStr(TimePickerStep.weekendsTo), 23);
            editor.putInt(getMinuteStr(TimePickerStep.weekendsTo), 0);
            editor.apply();
        }

        Button get = findViewById(R.id.button);
        get.setOnClickListener(v -> {
            IOTLabCalls.fetchMotes(this, 1, "light1");
        });
    }

    protected void onBack() {
        Log.d("MainActivity", "Reprise de l'activité");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        setContentView(R.layout.activity_main);

        TextView text1 = findViewById (R.id.textView2);
        ToggleButton switch1 = findViewById (R.id.toggleButton);
        text1.setText(isServiceRunning(MainService.class) ? "Running" : "Stopped");
        switch1.setChecked(isServiceRunning(MainService.class));
        switch1.setOnClickListener(v -> {
            if (switch1.isChecked()) {
                startService(new Intent(this, MainService.class));
            } else {
                stopService(new Intent(this, MainService.class));
            }
            text1.setText(switch1.isChecked() ? "Running" : "Stopped");
        });

        CheckBox checkbox1 = findViewById(R.id.checkBox);
        checkbox1.setChecked(prefs.getBoolean("serviceOnBoot", false));
        checkbox1.setOnClickListener(v -> {
            Log.d("MainActivity", "Start service at boot switched");
            editor.putBoolean("serviceOnBoot", checkbox1.isChecked());
            editor.apply();
        });

        Button get = findViewById(R.id.button);
        get.setOnClickListener(v -> {
            IOTLabCalls.fetchMotes(this, 1, "light1");
        });
    }

    public void onDestroy() {
        super.onDestroy();
        // stopService(new Intent(this, MainService.class));
    }

    @Override
    public void onResponse(@Nullable Motes motes) {
        if (motes != null) {
            LinearLayout motes_layout = findViewById (R.id.ltmotes);
            int i = 0;
            for (Mote mote: motes.getData()) {
                LinearLayout mote_layout = (LinearLayout) motes_layout.getChildAt(i);

                Log.d("MainActivity", String.format("Mote %s is %s (%.2f)", mote.getMote(), mote.getValue() > 250 ? "on" : "off", mote.getValue()));
                //Change content of layout:
                ((TextView) mote_layout.getChildAt(0)).setText(String.format("%1$TD %1$TT", new Timestamp(mote.getTimestamp())));
                ((TextView) mote_layout.getChildAt(1)).setText("Mote "+mote.getMote());
                ((TextView) mote_layout.getChildAt(2)).setText(String.format("%s (%.2f)", mote.getValue() > 250 ? "on" : "off", mote.getValue()));
                //Set visible
                mote_layout.setVisibility(View.VISIBLE);
                i++;
            }
        }
    }

    @Override
    public void onFailure() {
        Toast.makeText(this, "Une erreur est survenue", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu, menu);
        settingsItem = menu.findItem(R.id.action_settings);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                settingsItem.setVisible(false);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                setContentView(R.layout.activity_settings);
                getSupportFragmentManager().beginTransaction().add(R.id.amio_main_settings, new MainSettingsFragment()).addToBackStack(null).commit();
                return true;
            case android.R.id.home:
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                settingsItem.setVisible(true);
                setContentView(R.layout.activity_main);
                getSupportFragmentManager().popBackStack();
                onBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}