package com.example.projetamio;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("jsonschema2pojo")
public class Motes {

    @SerializedName("data")
    @Expose
    private List<Mote> data = null;

    public List<Mote> getData() {
        return data;
    }

    public void setData(List<Mote> data) {
        this.data = data;
    }

}