package com.example.projetamio;

import static com.example.projetamio.TimePickerFragment.getHourStr;
import static com.example.projetamio.TimePickerFragment.getMinuteStr;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

public class MainSettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Load the preferences from an XML resource
        setPreferencesFromResource(R.xml.main_preferences, rootKey);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());

        Preference weekdays = findPreference("weekdays_period");
        if ((prefs.getInt(getHourStr(TimePickerStep.weekdaysFrom), 0) == prefs.getInt(getHourStr(TimePickerStep.weekdaysTo), 0)) && (prefs.getInt(getMinuteStr(TimePickerStep.weekdaysFrom), 0) == prefs.getInt(getMinuteStr(TimePickerStep.weekdaysTo), 0))) {
            weekdays.setSummary("Disabled");
        } else {
            weekdays.setSummary(String.format("%02d:%02d - %02d:%02d",
                    prefs.getInt(getHourStr(TimePickerStep.weekdaysFrom), 0),
                    prefs.getInt(getMinuteStr(TimePickerStep.weekdaysFrom), 0),
                    prefs.getInt(getHourStr(TimePickerStep.weekdaysTo), 0),
                    prefs.getInt(getMinuteStr(TimePickerStep.weekdaysTo), 0)));
        }
        weekdays.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                DialogFragment fragFrom = new TimePickerFragment(TimePickerStep.weekdaysFrom, preference);
                fragFrom.show(getActivity().getSupportFragmentManager(), TimePickerStep.weekdaysFrom.name() + "TimePicker");
                return true;
            }
        });

        Preference weekends = findPreference("weekends_period");
        if ((prefs.getInt(getHourStr(TimePickerStep.weekendsFrom), 0) == prefs.getInt(getHourStr(TimePickerStep.weekendsTo), 0)) && (prefs.getInt(getMinuteStr(TimePickerStep.weekendsFrom), 0) == prefs.getInt(getMinuteStr(TimePickerStep.weekendsTo), 0))) {
            weekends.setSummary("Disabled");
        } else {
            weekends.setSummary(String.format("%02d:%02d - %02d:%02d",
                    prefs.getInt(getHourStr(TimePickerStep.weekendsFrom), 0),
                    prefs.getInt(getMinuteStr(TimePickerStep.weekendsFrom), 0),
                    prefs.getInt(getHourStr(TimePickerStep.weekendsTo), 0),
                    prefs.getInt(getMinuteStr(TimePickerStep.weekendsTo), 0)));
        }
        weekends.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                DialogFragment fragFrom = new TimePickerFragment(TimePickerStep.weekendsFrom, preference);
                fragFrom.show(getActivity().getSupportFragmentManager(), TimePickerStep.weekendsFrom.name() + "TimePicker");
                return true;
            }
        });
    }
}