package com.example.projetamio;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import androidx.fragment.app.DialogFragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceManager;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    TimePickerStep step;
    Preference pref;

    public TimePickerFragment(TimePickerStep step, Preference pref) {
        this.step = step;
        this.pref = pref;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
        int hour = prefs.getInt(getHourStr(step), c.get(Calendar.HOUR_OF_DAY));
        int minute = prefs.getInt(getMinuteStr(step), c.get(Calendar.MINUTE));

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(getHourStr(step), hourOfDay);
        editor.putInt(getMinuteStr(step), minute);
        editor.apply();
        switch (step) {
            case weekdaysFrom: {
                DialogFragment fragTo = new TimePickerFragment(TimePickerStep.weekdaysTo, pref);
                fragTo.show(getActivity().getSupportFragmentManager(), TimePickerStep.weekdaysTo.name() + "TimePicker");
                break;
            }
            case weekdaysTo: {
                if ((prefs.getInt(getHourStr(TimePickerStep.weekdaysFrom), 0) == prefs.getInt(getHourStr(TimePickerStep.weekdaysTo), 0)) && (prefs.getInt(getMinuteStr(TimePickerStep.weekdaysFrom), 0) == prefs.getInt(getMinuteStr(TimePickerStep.weekdaysTo), 0))) {
                    pref.setSummary("Disabled");
                } else {
                    pref.setSummary(String.format("%02d:%02d - %02d:%02d",
                            prefs.getInt(getHourStr(TimePickerStep.weekdaysFrom), 0),
                            prefs.getInt(getMinuteStr(TimePickerStep.weekdaysFrom), 0),
                            prefs.getInt(getHourStr(TimePickerStep.weekdaysTo), 0),
                            prefs.getInt(getMinuteStr(TimePickerStep.weekdaysTo), 0)));
                }
                break;
            }
            case weekendsFrom: {
                DialogFragment fragTo = new TimePickerFragment(TimePickerStep.weekendsTo, pref);
                fragTo.show(getActivity().getSupportFragmentManager(), TimePickerStep.weekendsTo.name() + "TimePicker");
                break;
            }
            case weekendsTo: {
                if ((prefs.getInt(getHourStr(TimePickerStep.weekendsFrom), 0) == prefs.getInt(getHourStr(TimePickerStep.weekendsTo), 0)) && (prefs.getInt(getMinuteStr(TimePickerStep.weekendsFrom), 0) == prefs.getInt(getMinuteStr(TimePickerStep.weekendsTo), 0))) {
                    pref.setSummary("Disabled");
                } else {
                    pref.setSummary(String.format("%02d:%02d - %02d:%02d",
                            prefs.getInt(getHourStr(TimePickerStep.weekendsFrom), 0),
                            prefs.getInt(getMinuteStr(TimePickerStep.weekendsFrom), 0),
                            prefs.getInt(getHourStr(TimePickerStep.weekendsTo), 0),
                            prefs.getInt(getMinuteStr(TimePickerStep.weekendsTo), 0)));
                }
                break;
            }
        }
    }

    public static String getHourStr(TimePickerStep step) {
        return step.name() + "Hour";
    }

    public static String getMinuteStr(TimePickerStep step) {
        return step.name() + "Minute";
    }
}