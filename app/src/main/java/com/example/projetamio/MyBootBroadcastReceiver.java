package com.example.projetamio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.preference.PreferenceManager;

public class MyBootBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("MyBootBroadcastReceiver", "Smartphone booted");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        boolean serviceOnBoot = prefs.getBoolean("serviceOnBoot", false);
        if (serviceOnBoot) {
            Intent n_intent = new Intent(context, MainService.class);
            context.startService(n_intent);
        }
    }
}